package com.example.demo.repository;


import java.util.Date;
import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.example.demo.entity.Report;

@Repository
public interface ReportRepository extends JpaRepository<Report, Integer> {
	//ReportRepository が JpaRepository を継承しており、findAllメソッドを実行している
	List<Report> findByUpdatedDateBetweenOrderByUpdatedDateDesc(Date start, Date end);

	List<Report> findByOrderByUpdatedDateDesc();
}