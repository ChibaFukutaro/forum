package com.example.demo.controller;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import com.example.demo.entity.Comment;
import com.example.demo.entity.Report;
import com.example.demo.service.CommentService;
import com.example.demo.service.ReportService;

@Controller
public class ForumController {

	@Autowired
	ReportService reportService;

	@Autowired
	CommentService commentService;

	// 投稿内容表示画面
	@GetMapping
	public ModelAndView top(@RequestParam(name = "startDate",required = false) String startDate,
			@RequestParam(name = "endDate",required = false) String endDate) {
		ModelAndView mav = new ModelAndView();
		List<Report> contentData;
		if (startDate == null && endDate == null) {
			// 投稿を全件取得
			contentData = reportService.findAllReport();
		} else {
			//現在日時取得
			Date dateObj = new Date();
			//フォーマット作成
			SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
			String dateStr = dateFormat.format(dateObj);

			String start;
			String end;

			if (startDate == null || startDate.isEmpty()) {
				start = "2020-01-01 00:00:00";
			} else {
				start = startDate + " 00:00:00";
			}

			if (endDate == null || endDate.isEmpty()) {
				end = dateStr;
			} else {
				end = endDate + " 23:59:59";
			}

			Date startTime = null;
			Date endTime = null;
			try {
				startTime = dateFormat.parse(start);
				endTime = dateFormat.parse(end);
			} catch (ParseException e) {
				e.printStackTrace();
			}
			

		    contentData = reportService.findReport(startTime, endTime);
		}

		// コメントを全件取得
		List<Comment> textData = commentService.findAllComment();
		// 画面遷移先を指定
		mav.setViewName("/top");
		
		mav.addObject("startDate", startDate);
		mav.addObject("endDate", endDate);
		// 投稿データオブジェクトを保管
		mav.addObject("contents", contentData);
		// コメントデータオブジェクトを保管
		mav.addObject("texts", textData);
		return mav;
	}

	// 新規投稿画面
	@GetMapping("/new")
	public ModelAndView newContent() {
		ModelAndView mav = new ModelAndView();
		// form用の空のentityを準備
		Report report = new Report();
		// 画面遷移先を指定
		mav.setViewName("/new");
		// 準備した空のentityを保管
		mav.addObject("formModel", report);
		return mav;
	}

	// 投稿処理
	@PostMapping("/add")
	public ModelAndView addContent(@ModelAttribute("formModel") Report report) {

		//現在日時を取得
		Date dateObj = new Date();

		report.setCreatedDate(dateObj);
		report.setUpdatedDate(dateObj);
		// 投稿をテーブルに格納
		reportService.saveReport(report);
		// rootへリダイレクト
		return new ModelAndView("redirect:/");
	}

	// 新規コメント投稿画面
	@GetMapping("/newText/{id}")
	public ModelAndView newText(@PathVariable Integer id) {
		ModelAndView mav = new ModelAndView();
		// form用の空のentityを準備
		Comment comment = new Comment();
		//URLをレポートIDにセット
		comment.setReportId(id);

		// 画面遷移先を指定
		mav.setViewName("/newText");
		// 準備した空のentityを保管
		mav.addObject("formModel", comment);
		return mav;
	}

	// コメント投稿処理
	@PostMapping("/addText")
	public ModelAndView addText(@ModelAttribute("formModel") Comment comment) {

		Date dateObj = new Date();

		comment.setCreatedDate(dateObj);
		comment.setUpdatedDate(dateObj);
		// 投稿をテーブルに格納
		commentService.saveComment(comment);
		
		
		Report report = reportService.editReport(comment.getReportId());
		report.setUpdatedDate(dateObj);
		reportService.saveReport(report);

		// rootへリダイレクト
		return new ModelAndView("redirect:/");
	}

	// 削除処理
	@DeleteMapping("/delete/{id}")
	public ModelAndView deleteContent(@PathVariable Integer id) {
		// テーブルからIDに一致する投稿を削除
		reportService.deleteReport(id);
		// rootへリダイレクト
		return new ModelAndView("redirect:/");
	}

	// コメント削除処理
	@DeleteMapping("/deleteText/{id}")
	public ModelAndView deleteText(@PathVariable Integer id) {
		// テーブルからIDに一致する投稿を削除
		commentService.deleteComment(id);
		// rootへリダイレクト
		return new ModelAndView("redirect:/");
	}

	//編集画面
	@GetMapping("/edit/{id}")
	public ModelAndView editContent(@PathVariable Integer id) {
		ModelAndView mav = new ModelAndView();
		// form用の既存のentityを準備
		Report report = reportService.editReport(id);
		// 準備したentityを保管
		mav.addObject("formModel", report);
		// 画面遷移先を指定
		mav.setViewName("/edit");
		return mav;
	}

	// 編集処理
	@PutMapping("/update/{id}")
	public ModelAndView updateContent(@PathVariable Integer id, @ModelAttribute("formModel") Report report) {
		// UrlParameterのidを更新するentityにセット
		report.setId(id);
		
		Date dateObj = new Date();
		report.setUpdatedDate(dateObj);
		// 編集した投稿を更新
		reportService.saveReport(report);
		// rootへリダイレクト
		return new ModelAndView("redirect:/");
	}

	//コメント編集画面
	@GetMapping("/editText/{id}")
	public ModelAndView editText(@PathVariable Integer id) {
		ModelAndView mav = new ModelAndView();
		// form用の既存のentityを準備
		Comment comment = commentService.editComment(id);
		// 準備したentityを保管
		mav.addObject("formModel", comment);
		// 画面遷移先を指定
		mav.setViewName("/editText");
		return mav;
	}

	//コメント編集処理
	@PutMapping("/updateText/{id}")
	public ModelAndView updateText(@PathVariable Integer id, @ModelAttribute("formModel") Comment comment) {
		// UrlParameterのidを更新するentityにセット
		comment.setId(id);

		// 編集した投稿を更新
		Date dateObj = new Date();
		comment.setUpdatedDate(dateObj);

		commentService.saveComment(comment);
		// rootへリダイレクト
		return new ModelAndView("redirect:/");
	}
}
